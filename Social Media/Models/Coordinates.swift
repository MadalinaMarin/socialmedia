//
//  Coordinates.swift
//  Social Media
//
//  Created by Madalina Marin on 20/12/2019.
//  Copyright © 2019 social-media. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Coordinates: Object, Mappable {
    
    @objc dynamic var latitude = 0.0
    @objc dynamic var longitude = 0.0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        latitude <- map["lat"]
        longitude <- map["lng"]
    }
    
}
