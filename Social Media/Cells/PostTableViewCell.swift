//
//  PostTableViewCell.swift
//  Social Media
//
//  Created by Madalina Marin on 19/12/2019.
//  Copyright © 2019 social-media. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {

    @IBOutlet weak var postTitleLabel: UILabel!
    @IBOutlet weak var postTextView: UITextView!
    
    @IBOutlet weak var userButton: UIButton!
    @IBOutlet weak var seeMoreButton: UIButton!
    private var post: Post?
    
    func setPost(post: Post) {
        self.post = post
        postTitleLabel.text = self.post?.title
        postTextView.text = self.post?.body
        postTitleLabel.numberOfLines = 0
        userButton.setTitle("@\((self.post?.user?.username)!)", for: UIControl.State.normal)
    }
    
    func setButtonTag(value: Int) {
        userButton.tag = value
        seeMoreButton.tag = value
    }

    func getPost() -> Post? {
        return post
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func didTapUserDetails(_ sender: Any) {
        
    }

}
