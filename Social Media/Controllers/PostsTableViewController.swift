//
//  PostsTableViewController.swift
//  Social Media
//
//  Created by Madalina Marin on 20/12/2019.
//  Copyright © 2019 social-media. All rights reserved.
//

import UIKit


class PostsTableViewController: UIViewController {
    
    @IBOutlet weak var postsTableView: UITableView!

    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    var posts: [Post] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        postsTableView.delegate = self
        postsTableView.dataSource = self

    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadingView.isHidden = false
        loadingView.startAnimating()
        
        self.getPosts();
    }
    
    func getPosts() {
        posts =  DBManager.getObjects(ofType: Post.self)
        if posts.count == 0 {
            DispatchQueue.global(qos: .background).async {
                ApiClient.shared.getUsers(completion: { (usersArray) in
                    if usersArray.count != 0 {
                        ApiClient.shared.getPosts(completion:{
                            (postsArray) in
                            if (postsArray.count != 0) {
                                DispatchQueue.main.async {
                                    self.posts = postsArray
                                    self.loadingView.stopAnimating()
                                    self.loadingView.isHidden = true
                                    self.postsTableView.reloadData()
                                }
                            } else {
                                print("Zero posts")
                            }
                        }) { (error) in
                            print(error)
                        }
                    }
                }) { (error) in
                    print(error)
                }
            }
        } else {
            self.loadingView.stopAnimating()
            self.loadingView.isHidden = true
            self.postsTableView.reloadData()
        }
        print("done")
    }
 
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "userProfileSegue") {
            let vc = segue.destination as! UserDetailsViewController
            if let button = sender as? UIButton {
                let indexPath = IndexPath(row: button.tag, section: 0)
                let cell = self.postsTableView.cellForRow(at: indexPath) as! PostTableViewCell
                guard let user = cell.getPost()?.getUser() else {
                    return
                }
                vc.setUser(user: user)
            }
        } else if (segue.identifier == "seeMoreSegue") {
            let vc = segue.destination as! PostDetailsViewController
            if let button = sender as? UIButton {
                let indexPath = IndexPath(row: button.tag, section: 0)
                let cell = self.postsTableView.cellForRow(at: indexPath) as? PostTableViewCell
                if(cell != nil)
                {
                    guard let post = cell!.getPost() else {
                        return
                    }
                    vc.setPost(post: post)
                }
            }
        }
    }

}

extension PostsTableViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:PostTableViewCell = (tableView.dequeueReusableCell(withIdentifier: Constants.postCellIdentifier) as! PostTableViewCell?)!
        
        cell.setPost(post: posts[indexPath.row])
        cell.setButtonTag(value: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 230
    }
    
}
