//
//  ApiClient.swift
//  Social Media
//
//  Created by Madalina Marin on 19/12/2019.
//  Copyright © 2019 social-media. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class ApiClient: NSObject {
    static let shared = ApiClient()
    
    func getBaseURL() -> String {
        let userDefaults = UserDefaults.standard
        var baseURLValue: String = userDefaults.value(forKey: Constants.userDefaultsBaseUrl) as? String ?? ""
        
        if baseURLValue.isEmpty {
            baseURLValue = Constants.NETWORK_BASE_URL
            userDefaults.setValue(baseURLValue, forKey: Constants.userDefaultsBaseUrl)
            userDefaults.synchronize()
        }
        
        return baseURLValue
    }
    
    //MARK:- Methods
    func getPosts(
               completion: @escaping ([Post]) -> Void,
               error: @escaping (Error) -> Void) {

        let headers: HTTPHeaders = [
            "Content-Type" : "application/json; charset=UTF-8",
        ]

        Alamofire.request(
            ApiClientURL.posts,
            method: .get,
            headers: headers).responseJSON { response in
                switch response.result {
                case .success(let jsonResponse):
                    if let responseArray = jsonResponse as? NSArray {
                        var posts = [Post]()
                        for dictionary in responseArray {
                            if let postDictionary = dictionary as? NSDictionary {
                                let post = Mapper<Post>().map(JSONObject:postDictionary)
                                DBManager.saveObject(object: post!)
                                posts.append(post!)
                            }
                        }
                        completion(posts)
                            
                    }
                    completion([])
                case .failure(let e):
                    print(e)
                    error(e)
                }
            }
    }
    
    //MARK:- Methods
    func getUsers(
        completion: @escaping ([User]) -> Void,
        error: @escaping (Error) -> Void) {
        
        let headers: HTTPHeaders = [
            "Content-Type" : "application/json; charset=UTF-8",
        ]
        
        Alamofire.request(
            ApiClientURL.users,
            method: .get,
            headers: headers).responseJSON { response in
                switch response.result {
                case .success(let jsonResponse):
                    if let responseArray = jsonResponse as? NSArray {
                        var users = [User]()
                        for dictionary in responseArray {
                            if let postDictionary = dictionary as? NSDictionary {
                                let user = Mapper<User>().map(JSONObject:postDictionary)
                                DBManager.saveObject(object: user!)
                                users.append(user!)
                            }
                        }
                        completion(users)
                        
                    }
                    completion([])
                case .failure(let e):
                    print(e)
                    error(e)
                }
        }
        
    }
    
    func getComments(
        completion: @escaping ([Comment]) -> Void,
        error: @escaping (Error) -> Void) {
        
        let headers: HTTPHeaders = [
            "Content-Type" : "application/json; charset=UTF-8",
        ]
        
        Alamofire.request(
            ApiClientURL.comments,
            method: .get,
            headers: headers).responseJSON { response in
                switch response.result {
                case .success(let jsonResponse):
                    if let responseArray = jsonResponse as? NSArray {
                        var comments = [Comment]()
                        for dictionary in responseArray {
                            if let postDictionary = dictionary as? NSDictionary {
                                let comment = Mapper<Comment>().map(JSONObject:postDictionary)
                                DBManager.saveObject(object: comment!)
                                comments.append(comment!)
                            }
                        }
                        completion(comments)
                        
                    }
                    completion([])
                case .failure(let e):
                    print(e)
                    error(e)
                }
        }
    }
    
}
