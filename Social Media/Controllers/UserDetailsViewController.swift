//
//  UserDetailsViewController.swift
//  Social Media
//
//  Created by Madalina Marin on 20/12/2019.
//  Copyright © 2019 social-media. All rights reserved.
//

import UIKit

class UserDetailsViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    var user: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.nameLabel.text = user?.username
        self.emailLabel.text = user?.email
        self.addressLabel.text = user?.address?.toString()
        self.websiteLabel.text = user?.website
        self.companyLabel.text = user?.company?.name
    }
    
    func setUser(user: User){
        if user.id != 0 {
            self.user = user
        }
    }

}
