//
//  RealmHelper.swift
//  Social Media
//
//  Created by Madalina Marin on 20/12/2019.
//  Copyright © 2019 social-media. All rights reserved.
//

import Foundation
import RealmSwift

class DBManager {
    
    static let sharedInstance = DBManager()
    
    private init() {
    }
    
    static func getObjects<T:Object>(ofType: T.Type)->[T] {
        let database = try! Realm()
        let realmResults = database.objects(T.self)
        return Array(realmResults)
    }

    static func getObjects<T:Object>(ofType: T.Type, filter:String)->[T] {
        let database = try! Realm()
        let realmResults = database.objects(T.self).filter(filter)
        let realmm = database.objects(User.self)
        print(realmResults.count)
        return Array(realmResults)
    }
    
    static func saveObject<T:Object>(object: T) {
        DispatchQueue.main.async {

            let database = try! Realm()
            try! database.write {
                database.add(object)
            }
        }
    }
    
    static func deleteAllFromDatabase()  {
        let database = try! Realm()

        try! database.write {
            database.deleteAll()
        }
    }
    
    static func deleteFromDb<T:Object>(object: T)   {
        let database = try! Realm()
        try! database.write {
            database.delete(object)
        }
    }
    
    static func addCommentToPost(comment: Comment, post: Post) {
        let database = try! Realm()
        try! database.write {
            post.comments.append(comment)
        }
    }
}

