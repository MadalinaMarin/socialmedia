//
//  Company.swift
//  Social Media
//
//  Created by Madalina Marin on 20/12/2019.
//  Copyright © 2019 social-media. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Company: Object, Mappable {
    
    @objc dynamic var name = ""
    @objc dynamic var catchPhrase = ""
    @objc dynamic var bs = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        catchPhrase <- map["catchPhrase"]
        bs <- map["bs"]

    }
    
}
