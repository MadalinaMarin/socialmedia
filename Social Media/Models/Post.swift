//
//  Post.swift
//  Social Media
//
//  Created by Madalina Marin on 20/12/2019.
//  Copyright © 2019 social-media. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Post: Object, Mappable {
    
    @objc dynamic var id = 0
    @objc dynamic var title = ""
    @objc dynamic var body = ""
    @objc dynamic var user: User?
    @objc dynamic private var userID = 0
    dynamic var comments = List<Comment>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        body <- map["body"]
        userID <- map["userId"]
        user = DBManager.getObjects(ofType: User.self, filter: "id == \(userID)").first
    }
    
    func getUser() -> User? {
        return user
    }
    
    
}

