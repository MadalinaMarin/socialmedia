//
//  Constants.swift
//  Social Media
//
//  Created by Madalina Marin on 19/12/2019.
//  Copyright © 2019 social-media. All rights reserved.
//

import Foundation

struct Constants {
    static let userDefaultsBaseUrl = "baseUrl"
    static let userDefaultsLoginModel = "loginModel"
    static let userDefaultsRequiredId = "requiredId"
    static let NETWORK_BASE_URL = "https://jsonplaceholder.typicode.com"
    static let commentCellIdentifier = "commentCell"
    static let postCellIdentifier = "postCell"
}
