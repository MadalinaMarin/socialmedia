//
//  Address.swift
//  Social Media
//
//  Created by Madalina Marin on 20/12/2019.
//  Copyright © 2019 social-media. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Address: Object, Mappable {
    
    @objc dynamic var street = ""
    @objc dynamic var suite = ""
    @objc dynamic var city = ""
    @objc dynamic var zipcode = ""
    @objc dynamic var coordinates: Coordinates?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        street <- map["street"]
        suite <- map["suite"]
        city <- map["city"]
        zipcode <- map["zipcode"]
        coordinates <- map["geo"]
    }

    func toString() -> String {
        return street + ", " + suite + ", " + city + ", " + zipcode
    }
    
}
