//
//  Comments.swift
//  Social Media
//
//  Created by Madalina Marin on 23/12/2019.
//  Copyright © 2019 social-media. All rights reserved.
//

import Foundation

import Foundation
import RealmSwift
import ObjectMapper

class Comment: Object, Mappable {
    
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var email = ""
    @objc dynamic var body = ""
    @objc private dynamic var postID = 0

    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["name"]
        name <- map["name"]
        email <- map["email"]
        body <- map["body"]
        postID <- map["postId"]
        let post = DBManager.getObjects(ofType: Post.self, filter: "id == \(postID)").first
        DBManager.addCommentToPost(comment: self, post: post!)
    }
    
}
