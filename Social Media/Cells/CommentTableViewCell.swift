//
//  CommentTableViewCell.swift
//  Social Media
//
//  Created by Madalina Marin on 20/12/2019.
//  Copyright © 2019 social-media. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    private var comment: Comment?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setComment(comment: Comment) {
        self.comment = comment
        usernameLabel.text = self.comment?.email
        commentLabel.text = self.comment?.body
        commentLabel.numberOfLines = 0
        titleLabel.text = self.comment?.name
        titleLabel.numberOfLines = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
