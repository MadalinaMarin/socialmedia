//
//  PostDetailsViewController.swift
//  Social Media
//
//  Created by Madalina Marin on 23/12/2019.
//  Copyright © 2019 social-media. All rights reserved.
//

import UIKit

class PostDetailsViewController: UIViewController {

    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    @IBOutlet weak var postLabel: UILabel!
    @IBOutlet weak var postBodyTextView: UITextView!
    @IBOutlet weak var commentsTableView: UITableView!
    
    var comments: [Comment] = []
    var post: Post?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commentsTableView.delegate = self
        commentsTableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        postLabel.text = self.post?.title
        postBodyTextView.text = self.post?.body
        postLabel.numberOfLines = 0
        loadingView.isHidden = false
       
        loadingView.startAnimating()
        self.getComments();
    }
    
    func setPost(post: Post) {
        self.post = post
    }
    
    func getComments() {
        if(post?.comments != nil && post?.comments.count != 0) {
            comments = Array((post?.comments)!)
            self.loadingView.stopAnimating()
            self.loadingView.isHidden = true
            self.commentsTableView.reloadData()
        }
        if comments.count == 0 {
            DispatchQueue.global(qos: .background).async {
                ApiClient.shared.getComments(completion: { (commentsArray) in
                    if (commentsArray.count != 0 && self.post != nil) {
                        self.comments = Array((self.post?.comments)!)
                        DispatchQueue.main.async {
                            self.loadingView.stopAnimating()
                            self.loadingView.isHidden = true
                            self.commentsTableView.reloadData()
                        }
                        
                    } else {
                        print("Zero comments")
                    }
                }) { (error) in
                    print(error)
                }
            }
            
        } else {
            self.loadingView.stopAnimating()
            self.loadingView.isHidden = true
            self.commentsTableView.reloadData()
        }
        print("done")
    }
}

extension PostDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:CommentTableViewCell = (tableView.dequeueReusableCell(withIdentifier: Constants.commentCellIdentifier) as! CommentTableViewCell?)!
        
        cell.setComment(comment: comments[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 230
    }
    
}
