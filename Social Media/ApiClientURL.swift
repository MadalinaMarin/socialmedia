//
//  ApiClient.swift
//  Social Media
//
//  Created by Madalina Marin on 19/12/2019.
//  Copyright © 2019 social-media. All rights reserved.
//

import Foundation
import Alamofire

let POSTS = "/posts"
let USERS = "/users"
let COMMENTS = "/comments"

enum ApiClientURL: URLConvertible {
    typealias RawValue = ApiClientURL
    
    case posts
    case users
    case comments

    func asURL() throws -> URL {
        switch self {
        case .posts:
            let urlString = Constants.NETWORK_BASE_URL + POSTS
            return try urlString.asURL()
            
        case .users:
            let urlString = Constants.NETWORK_BASE_URL + USERS
            return try urlString.asURL()
            
        case .comments:
            let urlString = Constants.NETWORK_BASE_URL + COMMENTS
            return try urlString.asURL()
        }
    }
}
