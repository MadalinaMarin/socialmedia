//
//  User.swift
//  Social Media
//
//  Created by Madalina Marin on 20/12/2019.
//  Copyright © 2019 social-media. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class User: Object, Mappable {
    
    @objc dynamic var id = 0
    @objc dynamic var username = ""
    @objc dynamic var email = ""
    @objc dynamic var phone = ""
    @objc dynamic var website = ""
    @objc dynamic var address: Address?
    @objc dynamic var company: Company?


    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        username <- map["username"]
        email <- map["email"]
        phone <- map["phone"]
        website <- map["website"]
        address <- map["address"]
        company <- map["company"]
    }
    //todo
    static func getObjects() -> [User] {
        return DBManager.getObjects(ofType: User.self) as [User]
    }
       
}
